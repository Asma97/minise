
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>

int main(void)
{
    struct dirent *de;  // Pointer sur le repértoire d'entrer

    // opendir() retourne un pointeur de type DIR
    DIR *dr = opendir(".");


     if (dr == NULL)  // opendir retourne null si elle ne peut pas ouvrir le fichier
    {
        printf("on ne peut pas ouvrir le dossier" );
        return 0;
    }

    // for readdir()
    while ((de = readdir(dr)) != NULL)
            {printf("%s\n", de->d_name); }

    closedir(dr);

    puts("Le dossier a ete ferme avec succes");
    return 0;
}
