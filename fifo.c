#include<stdio.h>
#include<string.h>
#include<conio.h>
int main()
{
    char pn[10][10],t[10];
    int arr[10],bur[10],st[10],ft[10],tat[10],wt[10],i,j,n,temp;
    int totwt=0,tottat=0;
setvbuf(stdout, NULL, _IONBF, 0);
//clrscr();
    printf("Enter the number of processes:");
    scanf("%d",&n);
    for(i=0; i<n; i++)
    {
        printf("Enter the ProcessName, Arrival Time& Burst Time:");
        scanf("%s%d%d*c",&pn[i],&arr[i],&bur[i]);
    }
    for(i=0; i<n; i++)
    {
        for(j=0; j<n; j++)
        {
            if(arr[i]<arr[j])
            {
                temp=arr[i];
                arr[i]=arr[j];
                arr[j]=temp;
                temp=bur[i];
                bur[i]=bur[j];
                bur[j]=temp;
                strcpy(t,pn[i]);
                strcpy(pn[i],pn[j]);
                strcpy(pn[j],t);
            }

        }
    }
    for(i=0; i<n; i++)
    {
        if(i==0)
            st[i]=arr[i];
        else
            st[i]=ft[i-1];
        wt[i]=st[i]-arr[i];
        ft[i]=st[i]+bur[i];
        tat[i]=ft[i]-arr[i];
    }
    printf("\nPname\tarrivaltime\texecutiontime\twaitingtime\tturnaroundtime\tstart\tfinish");
    for(i=0; i<n; i++)
    {
        printf("\n%s\t%5d\t\t%5d\t\t%5d\t\t%5d\t\t%5d\t\t%5d",pn[i],arr[i],bur[i],wt[i],tat[i],st[i],ft[i]);

        totwt+=wt[i];
        tottat+=tat[i];
    }
    printf("\nAverage Waiting time:%f",(float)totwt/n);
    printf("\nAverage Turn Around Time:%f",(float)tottat/n);
    return 0;
}
